### Anotações ###

* React

- É uma biblioteca Javascript para criação de interfaces de alta performance baseadas em componentes.
- Não é um framework completo como AngularJS (responsabiliza apenas na gestão de interface).
- Uma das famosas bibliotecas que adotou os princípios do SPA.

* Single Page Application

- Novo modelo de desenvolvimento de aplicações Web.
- Aplicações inteiramente contidas no browser que não precisam fazer requisições de novas páginas no servidor.
- Não é preciso retornar uma nova página do servidor a cada iteração do usuário, como as aplicações antigas. Mas isso nos trás alguns problemas.
Por exemplo, submit de um formulário contendo validações, é preciso manipular o DOM para retirar as validações antigas e acrescentar as novas.
Essa adaptação é chata e toma tempo. Com isso, na criação do React veio a sacada de aplicar o conceito de Reactive Render.

* Reactive Render

- A cada alteração no state (dados, informações), o React descarta a interface e renderiza novamente, libertando da necessidade de manipular o DOM.
Isso é algo preocupante por exemplo no quesito performance, na perca de dados contidos no formulário ou da posição do scroll.
Para evitar esses problemas e fazer com que esse conceito aconteça de forma performática, surgiu a idéia de Virtual DOM.

* Virtual DOM

- É uma representação leve na memória da interface para renderizar no browser. O Reactive Render quando atualiza a interface não é renderizada pro browser e sim para o Virtual DOM, ou seja, ele gera um novo Virtual DOM. Pra renderizar novamente no browser especificamente, ele compara o antigo com o novo Virtual DOM identificando quais as diferenças entre um e outro. Sabendo as únicas diferenças, ele manipula o DOM somente em pontos específicos, tornando se muito performático.

* Components

- É a possibilidade de você quebrar a sua interface em pequenas peças.
Toda sua interface é composta por essas peças, que são independentes e com um objetivo específico, ajudando na separação de responsabilidades. Componentes são reutilizáveis, podem ser utilizados em mais de um lugar e mais de uma vez, com isso trazendo facilidade na estruturação de interfaces. E também podemos compor n componentes para poder criar componentes mais complexos.

** Grandes benefícios do React:

- Declarativo (Reactive Rendering + Virtual DOM)
- Orientado a componentes (Tudo é componente)

* Component React

- Um objeto para se tornar um componente do React, além de extender de React.Component, é preciso que no mínimo essa classe tenha uma função render(). Essa função retorna a representação da sua interface. A melhor forma de declarar a interface dentro da função render é com JSX.


* JSX: nos permite utilizar marcações html dentro do javascript, para não ter que programar a estrutura da página usando funções do React como, React.createElement;
Obs.: contém notificação de erros de compilação quando há falhas na sintaxe html.
Obs.: atributo 'class' do html é subtituído pelo 'className' devido a palavra chave do ES6 para definição de classes 'Class'.
Obs.: import do módulo 'React' é necessário para o funcionamento do JSX.

* Babel: transpiler de ES6;
- transform-react-jsx: responsável por converter código javascript que contém código jsx;
- preset-es2015: responsável por converter código ES6 para ES5 (back-end), para o padrão CommonJS;
Obs.: contém notificação de warnings de import em desuso.

* Webpack: module bundler que permite que um código javascript back-end (Node.js) seja interpretado pelo browser;

* Exports
** import React, { Component } from 'react';
- React: export default
- Component: export não default

## Variáveis do React ##

* Variável padrão da classe Component  (único atributo que o React observa)
- this.state: variável que representa o estado do objeto (não renderiza novamente o componente);
- this.setState: função responsável por alterar o valor do estado (renderiza novamente o componente);

* Variável extras da classe Component
- this.props: variável que recebe os argumentos passados para o componente via html;

## Funções padrões da classe Component (React lifecycle) ##

* componentWillMount: executa antes da função render;
* componentDidMount: executa depois da função render;

### React Router ###

* Link: gera um <a> com um evento associado ao router mas não gera reload da página;

### Links ###

* http://react-toolbox.com/#/components
* https://js.coach/react


### Dificuldades ###

* Tratamento de erros de requisições assíncronas usando fetch api
* Tela de listagem com filtros, no caso quando há dois states (users e filters), coloco tudo no state?
* Dificuldade na criação de tests


* Redux (state container)

- Uma arquitetura para desenvolvimento de aplicações que complementa o React inspirado no Facebook Flux e Elm Lang.

** Pontos iguais ao Flux:

- Fluxo de dados unidirecional

** Pontos diferentes ao Flux

- Único store contendo o estado para toda a aplicação
- Não existe um Dispatcher central

** Fluxo de dados

- Action: É um objeto simples contendo o tipo de operação a ser executada e alguma carga útil ({type: SOME_TYPE, payload}).
- Reducer: Recebe como argumento o state anterior e a action. Realiza a mudança e retorna o novo state resultante da aplicação da action.
- Store: Não é um class mas um objeto acompanhado de vários métodos. É criado executando o rootReducer no estado inicial da aplicação.
- Dispatcher: Quando uma action precisa ser executada, o método dispatch() da store é invocado passando a action como parâmetro.
Em seguida, todos os listeners são informados de que o state mudou, e com isso renderizam os componentes correspondentes conforme o novo state.

*** Redux pode ser usado com qualquer framework Javascript.
É comum usar o mesmo junto com React pois ele permite que codifique a view como uma função do estado.
Foco do Redux é executar com segurança atualizações para o state com base em várias actions.




