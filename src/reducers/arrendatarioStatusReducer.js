import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function arrendatarioStatusReducer(state = initialState.arrendatarioStatus, action) {
    switch (action.type) {
        case types.LOAD_ARRENDATARIO_STATUS_SUCCESS:
            if (!action.arrendatarioStatus) {
                return []
            }
            return action.arrendatarioStatus
        default:
            return state;
    }
}