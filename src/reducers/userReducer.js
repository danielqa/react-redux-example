import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function userReducer(state = initialState.users, action) {
    switch (action.type) {
        case types.LOAD_USERS_SUCCESS:
            if (!action.users) {
                return []
            }
            return action.users
        case types.CREATE_USER_SUCCESS:
            return [];
        default:
            return state;
    }
}