import { combineReducers } from 'redux';
import users from './userReducer';
import arrendatarioStatus from './arrendatarioStatusReducer';
import session from './sessionReducer';

const rootReducer = combineReducers({
    users,
    arrendatarioStatus,
    session
})

export default rootReducer;