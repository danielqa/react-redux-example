import * as types from './actionTypes';
import userApi from '../api/userApi';
import { browserHistory } from 'react-router';
import { NotificationManager } from 'react-notifications';
import moment from 'moment';

export function loadUsers(filters) {
    let newFilters = Object.assign({}, filters);
    newFilters.arrendatarioDataCadastradoDataInicio = moment(newFilters.arrendatarioDataCadastradoDataInicio, "DD/MM/YYYY").valueOf();
    newFilters.arrendatarioDataCadastradoDataFim = moment(newFilters.arrendatarioDataCadastradoDataFim, "DD/MM/YYYY").valueOf();
    newFilters.usuarioAdministradorTelefone = newFilters.usuarioAdministradorTelefone ? newFilters.usuarioAdministradorTelefone.replace(/[^\d]+/g, '') : '';

    return dispatch => userApi.getAllUsers(newFilters).then(users => {
        dispatch({ type: types.LOAD_USERS_SUCCESS, users })
    }).catch(error => {
        throw (error);
    });
}

export function createUser(user) {
    let newUser = Object.assign({}, user);
    newUser.nomeDominio = newUser.nomeAssinante ? newUser.nomeAssinante.toLowerCase() : '';
    newUser.senhaConfirmacao = newUser.senha;
    newUser.justificativaSemCpfCnpj = 'sem cpf';
    newUser.tipoPessoa = 'FISICA';
    newUser.aceitouTermo = true;

    return dispatch => userApi.createUser(newUser)
        .then(handleErrors)
        .then(response => {
            browserHistory.push('/users');
            NotificationManager.success('Arrendatário cadastrado com sucesso.');
            dispatch({ type: types.CREATE_USER_SUCCESS })
        })
        .catch(error => {
            error.json().then(json => {
                if (error.status === 412) {
                    const chavesValidacao = json.validacao.map(obj => obj.itemValidacao).reduce((pre, cur) => pre.concat(cur)).map(obj => obj.chaveErro);
                    chavesValidacao.forEach(chave => {
                        const mensagem = getMessage(chave)
                        if (mensagem) {
                            NotificationManager.error(mensagem);
                        }
                    })
                }
            })
        });
}

function getMessage(chaveMensagem) {
    switch (chaveMensagem) {
        case 'erro.validacao.arrendatario.senha.nao.informado':
            return 'Informe a senha.';
        case 'erro.validacao.arrendatario.senha.fora.formado':
            return 'A senha deve conter ao menos 6 caracteres, com ao menos um número e uma letra.';
        case 'erro.validacao.arrendatario.senha.tamanhoMaximo':
            return 'Sua senha deve possuir no máximo 255 caracteres.';
        case 'erro.validacao.arrendatario.senha.tamanhoMinimo':
            return 'Sua senha deve possuir no mínimo 6 caracteres.';
        case 'erro.validacao.arrendatario.email.tamanhoMaximo':
            return 'O campo E-mail deve conter no máximo 255 caracteres.';
        case 'erro.validacao.arrendatario.email.invalido':
            return 'E-mail informado inválido.';
        case 'erro.validacao.arrendatario.email.naoInformado':
            return 'Informe o e-mail.';
        case 'erro.validacao.arrendatario.nome.tamanho.invalido':
            return 'O campo Nome do Assinante deve conter tamanho mínimo de 2 e máximo de 26 caracteres.';
        case 'erro.validacao.arrendatario.nome.nao.informado':
            return 'Informe o Nome do Assinante.';
        case 'erro.validacao.arrendatario.nome.duplicado':
            return 'Já existe um registro com este Nome.';
        case 'erro.validacao.arrendatario.dominio.nome.foraPadrao':
            return 'O Nome deve estar em caixa baixa e conter apenas letras, números ou traços. Não pode conter somente números e não pode começar e terminar com traço.';
        case 'erro.validacao.arrendatario.email.ja.existe':
            return 'Já existe uma conta para o e-mail informado. Favor verficar seu e-mail para maiores informações de recuperação.';
        case 'erro.validacao.arrendatario.aceitou.termo.invalido':
            return 'Confirme o termo de aceite.';
        default:
            return ''
    }
}

function handleErrors(response) {
    if (!response.ok) {
        throw response;
    }
    return response;
}