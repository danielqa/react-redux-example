import * as types from './actionTypes';
import sessionApi from '../api/sessionApi';
import { browserHistory } from 'react-router';
import auth from '../auth/authenticator';

export function loginUser(credentials) {
    return dispatch => sessionApi.login(credentials).then(response => {
        auth.logIn(response.access_token);
        browserHistory.push('/');
        dispatch({ type: types.LOG_IN_SUCCESS })
    }).catch(error => {
        throw (error);
    });
}

export function logoutUser() {
    var result = confirm("Deseja realmente sair do sistema?");
    if (result) {
        return dispatch => {
            auth.logOut();
            browserHistory.push('/login');
            dispatch({ type: types.LOG_OUT_SUCCESS })
        }
    }
}