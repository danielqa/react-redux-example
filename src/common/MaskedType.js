import React, { PropTypes } from 'react';
import MaskedInput from 'react-text-mask';

const MaskedType = ({ name, label, onChange, col, placeholder, value, mask }) => {
    let wrapperClass = 'form-group';
    if (col) {
        wrapperClass += ' ' + col;
    }

    return (
        <div className={wrapperClass}>
            <label htmlFor={name}>{label}</label>
            <MaskedInput mask={mask}
                name={name}
                className="form-control"
                placeholder={placeholder}
                value={value}
                onChange={onChange} />
        </div>
    );
};

MaskedType.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    col: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    mask: PropTypes.array.isRequired
};

export default MaskedType;