import React, { PropTypes } from 'react';

const InputType = ({ name, label, onChange, col, placeholder, value, type = "text" }) => {
    let wrapperClass = 'form-group';
    if (col) {
        wrapperClass += ' ' + col;
    }

    return (
        <div className={wrapperClass}>
            <label htmlFor={name}>{label}</label>
            <input
                type={type}
                name={name}
                className="form-control"
                placeholder={placeholder}
                value={value}
                onChange={onChange} />
        </div>
    );
};

InputType.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    col: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.string
};

export default InputType;