import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../actions/sessionActions';
import { Link } from 'react-router';

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
    }

    logout(event) {
        event.preventDefault();
        this.props.actions.logoutUser();
    }

    render() {
        return (
            <div className="jumbotron">
                <h1>React Example</h1>
                <p>Redux Architecture</p>
                {!sessionStorage.jwt
                    ? <Link to="login" className="btn btn-primary btn-lg">Login</Link>
                    : <button type="button" className="btn btn-default btn-lg" onClick={this.logout}>Logout</button>
                }
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

export default connect(null, mapDispatchToProps)(HomePage);