import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions/sessionActions';
import InputType from '../common/InputType';

class LogInPage extends Component {
    constructor(props) {
        super(props);
        this.state = { credentials: { email: 'Site-Adv', password: 'site-adv@2015@zxc3' } }
        this.onChange = this.onChange.bind(this);
        this.login = this.login.bind(this);
    }

    onChange(event) {
        const field = event.target.name;
        const credentials = this.state.credentials;
        credentials[field] = event.target.value;
        return this.setState({ credentials: credentials });
    }

    login(event) {
        event.preventDefault();
        this.props.actions.loginUser(this.state.credentials);
    }

    render() {
        return (
            <div>
                <form>
                    <InputType
                        name="email"
                        label="Usuário"
                        value={this.state.credentials.email}
                        onChange={this.onChange} />

                    <InputType
                        name="password"
                        label="Senha"
                        type="password"
                        value={this.state.credentials.password}
                        onChange={this.onChange} />

                    <input
                        type="submit"
                        className="btn btn-primary"
                        value="Login"
                        onClick={this.login} />
                    {" "}
                </form>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

export default connect(null, mapDispatchToProps)(LogInPage);