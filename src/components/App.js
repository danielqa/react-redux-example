import React from 'react';
import Header from '../common/Header';
import { NotificationContainer } from 'react-notifications';

const Main = React.createClass({
    render() {
        return (
            <div className="container">
                <Header />
                <NotificationContainer />
                {this.props.children}
            </div>
        );
    }
});

export default Main;