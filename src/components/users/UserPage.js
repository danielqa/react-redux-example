import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import UserForm from './UserForm';

class UserPage extends Component {
    constructor(props) {
        super(props);
        this.updateUserState = this.updateUserState.bind(this);
        this.saveUser = this.saveUser.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.user.arrendatarioCodigo !== nextProps.user.arrendatarioCodigo) {
            this.setState({ user: nextProps.user });
        }
    }

    updateUserState(event) {
        const field = event.target.name;
        const user = this.state.user;
        user[field] = event.target.value;
        return this.setState({ user: user });
    }

    saveUser(event) {
        event.preventDefault();
        this.props.updateUser(this.state.user);
    }

    render() {
        return (
            <div>
                <h1>Editar Arrendatário</h1>
                <UserForm
                    user={this.state.user}
                    onSave={this.saveUser}
                    onChange={this.updateUserState} />
            </div>
        );
    }
}

UserPage.propTypes = {
    user: PropTypes.object.isRequired
};

function getUserById(users, arrendatarioCodigo) {
    let user = users.find(user => user.arrendatarioCodigo === arrendatarioCodigo)
    return Object.assign({}, user)
}

function mapStateToProps(state, ownProps) {
    let user = {
        nomeAssinante: '', email: '', senha: '', numeroTelefone: ''
    };
    const userId = ownProps.params.arrendatarioCodigo;
    if (userId && state.users.length > 0) {
        user = getUserById(state.users, ownProps.params.arrendatarioCodigo);
    }
    return { user: user };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);