import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as courseActions from '../../actions/userActions';
import UserForm from './UserForm';

class NewUserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                nomeAssinante: '',
                email: '',
                senha: ''
            },
            saving: false
        };
        this.saveUser = this.saveUser.bind(this);
        this.updateUserState = this.updateUserState.bind(this);
    }

    updateUserState(event) {
        const field = event.target.name;
        const user = this.state.user;
        user[field] = event.target.value;
        return this.setState({ user: user });
    }

    saveUser(event) {
        event.preventDefault();
        this.props.actions.createUser(this.state.user)
    }

    render() {
        return (
            <div>
                <h1>Novo Arrendatário</h1>
                <UserForm
                    user={this.state.user}
                    onSave={this.saveUser}
                    onChange={this.updateUserState} />
            </div>
        );
    }
}

NewUserPage.propTypes = {
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(courseActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewUserPage);