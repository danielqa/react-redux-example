import React, { Component, PropTypes } from 'react';
import MaskedInput from 'react-text-mask';
import InputType from '../../common/InputType';
import MaskedType from '../../common/MaskedType';

class UserFilters extends Component {
    render() {
        const maskTelefone = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
        const maskData = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];

        return (
            <div className="row">
                <div className="from-group col-md-4">
                    <label>Data cadastro</label>
                    <div className="input-group">
                        <MaskedInput mask={maskData}
                            name="arrendatarioDataCadastradoDataInicio"
                            className="form-control"
                            value={this.props.filters.arrendatarioDataCadastradoDataInicio}
                            onChange={this.props.onChange} />

                        <span className="input-group-addon">
                            <i className="glyphicon glyphicon-calendar"></i>
                        </span>

                        <MaskedInput mask={maskData}
                            name="arrendatarioDataCadastradoDataFim"
                            className="form-control"
                            value={this.props.filters.arrendatarioDataCadastradoDataFim}
                            onChange={this.props.onChange} />
                    </div>
                </div>

                <InputType
                    name="arrendatarioCodigo"
                    label="Código arrendatário"
                    col="col-md-2"
                    value={this.props.filters.arrendatarioCodigo}
                    onChange={this.props.onChange} />

                <div className="form-group col-md-2">
                    <label>Status</label>
                    <select
                        className="form-control"
                        name="arrendatarioStatus"
                        value={this.props.filters.arrendatarioStatus}
                        onChange={this.props.onChange}>
                        <option value=""></option>
                        {this.props.arrendatarioStatus.map(status =>
                            <option key={status.chave} value={status.chave}>{status.valor}</option>
                        )}
                    </select>
                </div>

                <InputType
                    name="arrendatarioNome"
                    label="Nome arrendatário"
                    col="col-md-3"
                    value={this.props.filters.arrendatarioNome}
                    onChange={this.props.onChange} />

                <div className="form-group col-md-1">
                    <label>&nbsp;</label>
                    <button className="btn btn-primary col-md-12" type="button" onClick={this.props.find}>Buscar</button>
                </div>

                <InputType
                    name="usuarioAdministradorNome"
                    label="Nome usuário"
                    col="col-md-3"
                    value={this.props.filters.usuarioAdministradorNome}
                    onChange={this.props.onChange} />

                <InputType
                    name="usuarioEmail"
                    type="email"
                    label="E-mail"
                    col="col-md-3"
                    value={this.props.filters.usuarioEmail}
                    onChange={this.props.onChange} />

                <MaskedType
                    name="usuarioAdministradorTelefone"
                    mask={maskTelefone}
                    label="Telefone"
                    col="col-md-2"
                    value={this.props.filters.usuarioAdministradorTelefone}
                    onChange={this.props.onChange} />

                <div className="form-group col-md-3">
                    <label></label>
                    <div className="checkbox">
                        <label>
                            <input
                                type="checkbox"
                                name="arrendatarioAtivos"
                                value={this.props.filters.arrendatarioAtivos}
                                onChange={this.props.onChange} />Somente arrendatários ativos
                        </label>
                    </div>
                </div>
            </div>
        );
    }
};

UserFilters.propTypes = {
    filters: PropTypes.object.isRequired,
    arrendatarioStatus: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    find: PropTypes.func.isRequired
};

export default UserFilters;