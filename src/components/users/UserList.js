import React, { PropTypes } from 'react';
import moment from 'moment';
// import { Link } from 'react-router';

const UserList = ({ users }) => {
    return (
        <div className="table-responsive">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>CÓDIGO</th>
                        <th>ARRENDATÁRIO</th>
                        <th>DATA CADASTRO</th>
                        <th>ÚLTIMO LOGIN</th>
                        <th>NOME ADMIN</th>
                        <th>CPF/CNPJ</th>
                        <th>TELEFONE</th>
                        <th>E-MAIL</th>
                        <th>USUÁRIOS ATIVOS</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    {users.length
                        ? users.map(user =>
                            <tr key={user.arrendatarioCodigo}>
                                <td>{user.arrendatarioCodigo}</td>
                                <td>{user.arrendatarioNome}</td>
                                <td>{user.arrendatarioDataCadastro ? moment(user.arrendatarioDataCadastro).format('DD/MM/YYYY') : '(Não informado)'}</td>
                                <td>{user.arrendatarioDataUltimoLogin ? moment(user.arrendatarioDataUltimoLogin).format('DD/MM/YYYY') : '(Não informado)'}</td>
                                <td>{user.usuarioAdministradorNome ? user.usuarioAdministradorNome : '(Não informado)'}</td>
                                <td>{user.usuarioAdministradorCpfCnpj ? user.usuarioAdministradorCpfCnpj : '(Não informado)'}</td>
                                <td>{user.pessoaTelefoneWs && user.pessoaTelefoneWs[0] ? user.pessoaTelefoneWs[0].telefone.numeroTelefone : '(Não informado)'}</td>
                                <td>{user.pessoaEmailWs && user.pessoaEmailWs[0] ? user.pessoaEmailWs[0].email : '(Não informado)'}</td>
                                <td>{user.arrendatarioUsuarioQuantidadeAtivos}</td>
                                <td>{user.arrendatarioStatus}</td>
                            </tr>
                        ) :
                        <tr>
                            <td colSpan="10">
                                <span>Não existe informação para a busca executada</span>
                            </td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    );
};

UserList.propTypes = {
    users: PropTypes.array.isRequired
};

export default UserList;

/*<div>
    {users.map(user =>
        <div className="bs-callout bs-callout-info" key={user.arrendatarioCodigo}>
            <h4 className="col-md-11">{user.arrendatarioNome}</h4>
            <Link to={'/users/' + user.arrendatarioCodigo} className="btn btn-default">Editar</Link>
            <button onClick={() => deleteUser(user)} className="btn btn-default">Excluir</button>
        </div>
    )}
</div>*/