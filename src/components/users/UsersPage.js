import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import UserFilters from './UserFilters';
import UserList from './UserList';
import { NotificationManager } from 'react-notifications';
import * as userActions from '../../actions/userActions';
import * as arrendatarioStatusActions from '../../actions/arrendatarioStatusActions';

class UsersPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filters: {}
        };
        this.updateFilterState = this.updateFilterState.bind(this);
        this.findUsers = this.findUsers.bind(this);
    }

    componentWillMount() {
        this.props.loadArrendatarioStatus();
    }

    updateFilterState(event) {
        const field = event.target.name;
        const filters = this.state.filters;
        let value = event.target.value;
        if (field === 'arrendatarioStatus') {
            value = value ? [value] : [];
        }
        if (field === 'arrendatarioAtivos') {
            value = event.target.checked;
        }
        filters[field] = value;
        // return this.setState({ filters: filters });
    }

    findUsers(event) {
        event.preventDefault();
        const validFilters = this.validateFilters(this.state.filters);
        if (validFilters) {
            this.props.loadUsers(this.state.filters);
        }
    }

    validateFilters(filters) {
        if (filters.arrendatarioDataCadastradoDataInicio && !moment(filters.arrendatarioDataCadastradoDataInicio, "DD/MM/YYYY").isValid()) {
            NotificationManager.error('A data cadastro inicial é inválida.');
            return false;
        }
        if (filters.arrendatarioDataCadastradoDataFim && !moment(filters.arrendatarioDataCadastradoDataFim, "DD/MM/YYYY").isValid()) {
            NotificationManager.error('A data cadastro final é inválida.');
            return false;
        }
        return true;
    }

    render() {
        return (
            <div>
                <h1>Arrendatários <Link to={'/users/new'} className="btn btn-primary">+</Link></h1>
                <UserFilters filters={this.props.filters} arrendatarioStatus={this.props.arrendatarioStatus} onChange={this.updateFilterState} find={this.findUsers} />
                <UserList users={this.props.users} />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        users: state.users,
        arrendatarioStatus: state.arrendatarioStatus,
        filters: {}
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...userActions, ...arrendatarioStatusActions }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);