import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import InputType from '../../common/InputType';
import MaskedType from '../../common/MaskedType';

class UserForm extends Component {
    render() {
        const maskTelefone = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

        return (
            <form>
                <InputType
                    name="nomeAssinante"
                    label="Nome do Assinante"
                    value={this.props.user.nomeAssinante}
                    onChange={this.props.onChange} />

                <InputType
                    name="email"
                    label="Email (Login de acesso ao SAJ ADV)"
                    value={this.props.user.email}
                    onChange={this.props.onChange} />

                <InputType
                    name="senha"
                    type="password"
                    label="Senha"
                    value={this.props.user.senha}
                    onChange={this.props.onChange} />

                <MaskedType
                    name="numeroTelefone"
                    mask={maskTelefone}
                    label="Telefone"
                    value={this.props.user.numeroTelefone}
                    onChange={this.props.onChange} />

                <input
                    type="submit"
                    disabled={this.props.saving}
                    value={this.props.saving ? 'Salvando...' : 'Salvar'}
                    className="btn btn-primary"
                    onClick={this.props.onSave} />

                <Link to={'/users'} className="btn btn-primary">Cancelar</Link>
            </form>
        );
    }
}

UserForm.propTypes = {
    user: PropTypes.object.isRequired,
    onSave: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired
};

export default UserForm;