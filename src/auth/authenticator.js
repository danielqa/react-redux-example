class Auth {
    static loggedIn() {
        return !!sessionStorage.getItem('jwt');
    }

    static logIn(access_token) {
        sessionStorage.setItem('jwt', access_token);
    }

    static logOut() {
        sessionStorage.removeItem('jwt');
    }
}

export default Auth;