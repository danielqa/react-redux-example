import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import HomePage from './components/home/HomePage';
import UsersPage from './components/users/UsersPage';
import UserPage from './components/users/UserPage';
import NewUserPage from './components/users/NewUserPage';
import LogInPage from './components/LogInPage';
import auth from './auth/authenticator';

export default (
    <Route path="/" component={App}>
        <IndexRoute component={HomePage} />
        <Route path="/login" component={LogInPage} />
        <Route path="/users" component={UsersPage} onEnter={requireAuth} />
        <Route path="/users/new" component={NewUserPage} />
        <Route path="/users/:hash" component={UserPage} />
    </Route>
);

function requireAuth(nextState, replace) {
    if (!auth.loggedIn()) {
        replace({
            pathname: '/login',
            state: { nextPathname: nextState.location.pathname }
        })
    }
}