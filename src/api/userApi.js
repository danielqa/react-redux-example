class UserApi {
    static getAllUsers(filters) {

        const url = new URL('http://localhost:8080/adv-service/public/admin/arrendatario/consulta'),
            params = {
                'pagina': 0,
                'quan-registros': 150
            }
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

        const request = new Request(url, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            }),
            body: JSON.stringify(filters)
        });

        return fetch(request)
            .then(response => response.json())
            .then(json => json.arrendatarioConsultaResultadoWs)
    }

    static createUser(user) {
        const request = new Request('http://localhost:8080/adv-service/site-adv/arrendatario', {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            }),
            body: JSON.stringify(user)
        });

        return fetch(request);
    }

    static getArrendatarioStatus() {

        const request = new Request('http://localhost:8080/adv-service/public/admin/arrendatario-status', {
            method: 'GET',
            headers: new Headers({
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            })
        });

        return fetch(request)
            .then(response => response.json())
            .then(json => json.simpleDto)
    }
}

export default UserApi;