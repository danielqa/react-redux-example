class SessionApi {
    static login(credentials) {

        const params = {
            'grant_type': 'client_credentials'
        };

        const bodyParams = Object.keys(params).map((key) => {
            return key + '=' + params[key];
        }).join('&');

        const request = new Request('http://localhost:8080/adv-bouncer-authorization-server/oauth/token', {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Basic ' + btoa(credentials.email + ':' + credentials.password)
            }),
            body: bodyParams
        });

        return fetch(request).then(response => response.json())
    }
}

export default SessionApi;